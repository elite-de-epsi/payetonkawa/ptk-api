import { Controller, Post, Body, HttpStatus } from '@nestjs/common';
import { AuthService } from '../service/auth.service';
import { SendEmailAuthPayload } from '../payload/send-email.auth.payload';
import { CheckCodeAuthPayload } from '../payload/check-code.auth.payload';
import { AuthTokenDto } from '../dto/auth.token.dto';
import { ApiTags } from '@nestjs/swagger';

@Controller('auth')
@ApiTags('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  /**
   * Method which permit to send a sms verification code
   * Token is required.
   *
   * @param payload
   * @return {number} - Customers which correspond
   */
  @Post('send-email')
  async sendEmailVerificationCode(
    @Body() payload: SendEmailAuthPayload,
  ): Promise<number> {
    await this.authService.sendEmailVerificationCode(payload.email);
    return HttpStatus.OK;
  }

  /**
   * Method which permit to check the code sent by email.
   *
   * @param payload
   * @return {AuthTokenDto} - New token with new permissions.
   */
  @Post('check-code')
  async checkCode(
    @Body() payload: CheckCodeAuthPayload,
  ): Promise<AuthTokenDto> {
    return await this.authService.checkEmailVerificationCode(
      payload.email,
      payload.code,
    );
  }
}
