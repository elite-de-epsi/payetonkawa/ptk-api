import { Test, TestingModule } from '@nestjs/testing';
import { AuthService } from './auth.service';
import { HttpModule, HttpService } from '@nestjs/axios';
import { of } from 'rxjs';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { AuthModule } from '../auth.module';
import { AuthMockData } from './../auth.mock';

describe('AuthService', () => {
  let authService: AuthService;
  let httpService: HttpService;
  let configService: ConfigService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [ConfigModule.forRoot(), AuthModule, HttpModule],
      providers: [
        AuthService,
        {
          provide: HttpService,
          useValue: {
            post: jest.fn(),
          },
        },
        {
          provide: ConfigService,
          useValue: {
            get: jest.fn(),
          },
        },
      ],
    }).compile();

    authService = module.get<AuthService>(AuthService);
    httpService = module.get<HttpService>(HttpService);
    configService = module.get<ConfigService>(ConfigService);
  });

  it('should be defined', () => {
    expect(authService).toBeDefined();
  });

  describe('sendEmailVerificationCode', () => {
    it('should call the Auth0 API and send an email verification code', async () => {
      jest
        .spyOn(httpService, 'post')
        .mockImplementation(() => of({ data: {} } as any));
      jest.spyOn(configService, 'get').mockReturnValue('test_value');

      await authService.sendEmailVerificationCode(AuthMockData.email);

      expect(httpService.post).toHaveBeenCalled();
    });
  });

  describe('checkEmailVerificationCode', () => {
    it('should call the Auth0 API and check the email verification code', async () => {
      jest
        .spyOn(httpService, 'post')
        .mockImplementation(() =>
          of({ data: AuthMockData.authTokenDto } as any),
        );
      jest.spyOn(configService, 'get').mockReturnValue('test_value');

      const result = await authService.checkEmailVerificationCode(
        AuthMockData.email,
        AuthMockData.code,
      );

      expect(httpService.post).toHaveBeenCalled();
      expect(result).toEqual(AuthMockData.authTokenDto);
    });
  });
});
