import { Test, TestingModule } from '@nestjs/testing';
import { AuthController } from './auth.controller';
import { AuthService } from '../service/auth.service';
import { HttpStatus } from '@nestjs/common';
import { AuthModule } from '../auth.module';
import { AuthMockData } from './../auth.mock';

describe('AuthController', () => {
  let authController: AuthController;
  let authService: AuthService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [AuthModule],
      controllers: [AuthController],
      providers: [
        {
          provide: AuthService,
          useValue: {
            sendEmailVerificationCode: jest.fn(),
            checkEmailVerificationCode: jest.fn(),
          },
        },
      ],
    }).compile();

    authController = module.get<AuthController>(AuthController);
    authService = module.get<AuthService>(AuthService);
  });

  it('should be defined', () => {
    expect(authController).toBeDefined();
  });

  describe('sendEmailVerificationCode', () => {
    it('should call sendEmailVerificationCode and return HttpStatus.OK', async () => {
      jest
        .spyOn(authService, 'sendEmailVerificationCode')
        .mockImplementation(() => Promise.resolve(undefined));

      const result = await authController.sendEmailVerificationCode(
        AuthMockData.sendEmailAuthPayload,
      );

      expect(result).toEqual(HttpStatus.OK);
    });
  });

  describe('checkCode', () => {
    it('should call checkEmailVerificationCode and return an AuthTokenDto', async () => {
      jest
        .spyOn(authService, 'checkEmailVerificationCode')
        .mockImplementation(() => Promise.resolve(AuthMockData.authTokenDto));

      const result = await authController.checkCode(
        AuthMockData.checkCodeAuthPayload,
      );

      expect(result).toEqual(AuthMockData.authTokenDto);
    });
  });
});
