import { HttpException, Injectable } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { AxiosRequestConfig } from 'axios';
import { AuthTokenDto } from '../dto/auth.token.dto';
import { firstValueFrom, Observable } from 'rxjs';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class AuthService {
  constructor(
    private httpService: HttpService,
    private configService: ConfigService,
  ) {}

  private httpOptions: AxiosRequestConfig = {
    headers: { 'Content-Type': 'application/json' },
  };
  private async executeApiCall<T>(apiCall: Observable<any>): Promise<T> {
    let response = null;
    try {
      response = await firstValueFrom(apiCall);
    } catch (e) {
      console.log(e);
      if (e.response) {
        throw new HttpException(
          e.response.data.error_description,
          e.response.status,
        );
      } else {
        throw new HttpException(e.message, 500);
      }
    }

    return response.data;
  }

  async sendEmailVerificationCode(email: string): Promise<void> {
    await this.executeApiCall(
      this.httpService.post(
        `${this.configService.get(
          'AUTH0_AUTHENTICATION_API',
        )}passwordless/start`,
        {
          connection: 'email',
          email: email,
          send: 'code',
          client_id: this.configService.get('BAACKEND_CLIENT_ID'),
          client_secret: this.configService.get('BAACKEND_CLIENT_SECRET'),
        },
        this.httpOptions,
      ),
    );
  }

  async checkEmailVerificationCode(
    email: string,
    code: string,
  ): Promise<AuthTokenDto> {
    return await this.executeApiCall<AuthTokenDto>(
      this.httpService.post(
        `${this.configService.get('AUTH0_AUTHENTICATION_API')}oauth/token`,
        {
          grant_type: 'http://auth0.com/oauth/grant-type/passwordless/otp',
          username: email,
          otp: code,
          realm: 'email',
          audience: this.configService.get('AUTH0_DRIING_API'),
          client_id: this.configService.get('BAACKEND_CLIENT_ID'),
          client_secret: this.configService.get('BAACKEND_CLIENT_SECRET'),
          scope: 'offline_access',
        },
        this.httpOptions,
      ),
    );
  }
}
